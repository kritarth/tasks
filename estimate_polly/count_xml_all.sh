#!/bin/bash
# use1

declare -a REGION=("use1" "usw1" "eu1" "apse1" "apse2" "sa1")
character_count () {
	count=`grep -E -o "\[Speak\] .* \{" $1.txt |wc -m`
	extra=`wc -l $1.txt |cut -d' ' -f1`
	let "final = count - extra * 11"
	echo "$final characters in $1"
}
for i in "${REGION[@]}"
do
   character_count "$i"
done
