#!/bin/bash

# 2020-01-08
DATE="${1:-2023-03-02}"
# use1
REGION="${2:-apse2}"

LANG_REGEX="$3"

c=$(cat ~/$DATE/$REGION.txt |sort |uniq |grep -P "$LANG_REGEX" |rev |cut -d'=' -f1 |rev |awk '{ sum += $1 } END { print sum }')
echo -e "$DATE/$REGION    \t$c"
