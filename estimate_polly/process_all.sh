#!/bin/bash

DATE=${1:-2023-03-01}
declare -a REGION=("use1" "usw1" "sa1" "eu1" "aps1" "apse1" "apse2")
for i in "${REGION[@]}"
do
	#./speak_custom.sh $DATE $i
	./speak_api.sh $DATE $i
	./speak_xml.sh $DATE $i
done
