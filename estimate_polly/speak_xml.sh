#!/bin/bash

# 2020-01-08
DATE="${1:-2023-03-02}"
# use1
REGION="${2:-apse2}"

mkdir /home/kritarth/$DATE/

files=`find /backup-logs/$REGION/ -maxdepth 3 -type f |grep $DATE |grep plivo.log`
echo "$files"
echo "found $(wc -l <<<$files) files"
echo "$files" |parallel -P 2 "zstdgrep -a -P \"\[Speak\] .*, 'loop': [0-9]+}\" {} ; echo 'DONE {}' >> /dev/stderr" > /home/kritarth/$DATE/$REGION.xml.txt
sort /home/kritarth/$DATE/$REGION.xml.txt |uniq > /home/kritarth/$DATE/sorted_$REGION.xml.txt
mv /home/kritarth/$DATE/sorted_$REGION.xml.txt /home/kritarth/$DATE/$REGION.xml.txt

./count.sh $DATE $REGION
