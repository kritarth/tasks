#!/bin/bash
# use1

DATE=$1

declare -a REGION=("aps1" "use1" "usw1" "eu1" "apse1" "apse2" "sa1")
character_count () {
	echo "COUNTING $1"
	grep -E -o "\[Speak\] .*}" ~/${DATE}/$1.xml.txt \
		|sed "s/\"/'/g" |sed 's/\[Speak\] /{"text": "/' |sed "s/ {'/\", \"/" |sed "s/', '/\", \"/g" \
		|sed "s/': '/\": \"/g" |sed "s/': /\": /g" |sed "s/\\\/X/g" \
		|sort |uniq \
		|jq 'select(.language as $a | ["en-US", "en-CA"] | index($a))' \
		|jq 'select(.voice |test("^Polly")|not)' \
		|jq '.text |length' \
		|awk '{ sum += $1 } END { print sum }'
}
for i in "${REGION[@]}"
do
   character_count "$i"
done
