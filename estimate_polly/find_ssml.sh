#!/bin/bash

FILE=$1

function urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; } ; export -f urldecode ;

function process() {
	data=$1
	data=$(sed -E "s/apikey=[a-zA-Z0-9]+//g" <<<"$data")
	data=$(urldecode "$data")
	speak=${data#"<speak>"}
	speak=${speak/\<\/speak\>*/}
	data="$data&chars=${#speak}"
	echo $data
} ; export -f process ;


zstdgrep -Po "ssml\?ssml=\K%3Cspeak%3E[^&]*[=&a-zA-Z0-9]*" $FILE |sort |uniq |parallel -P 4 process {}
