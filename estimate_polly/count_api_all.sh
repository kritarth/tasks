#!/bin/bash

grep -o "{.*}" $1 |grep "text" |sed "s/': u'/\": \"/g" |sed "s/': u\"/\": \"/g" |sed "s/', '/\", \"/g" |sed "s/\", '/\", \"/g" | sed "s/{'/{\"/g" |sed "s/'}/\"}/g" |sed "s/\\\x/ha/g" |jq .text |sed 's/"//g' |tr -d \\n |wc -m
