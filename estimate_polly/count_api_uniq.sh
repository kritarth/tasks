#!/bin/bash
# use1

DATE=$1

declare -a REGION=("aps1" "use1" "usw1" "eu1" "apse1" "apse2" "sa1")
character_count () {
	echo "COUNTING $1"
	grep -E -o "{.*}" ~/${DATE}/$1.api.txt \
		|grep "text" \
		|sed "s/': u'/\": \"/g" |sed "s/': u\"/\": \"/g" |sed "s/', '/\", \"/g" |sed "s/\", '/\", \"/g" | sed "s/{'/{\"/g" |sed "s/'}/\"}/g" |sed "s/\\\x/ha/g" \
		|sort |uniq \
		|jq '.text |length' \
		|awk '{ sum += $1 } END { print sum }'
		#|jq 'select(.voice |test("^Polly")|not)' \
		#|jq 'select(.language as $a | [null, "en-US", "en-CA"] | index($a))' \
		#|jq 'select(.voice as $a | [null, "MAN", "WOMAN"] | index($a))' \
	}

for i in "${REGION[@]}"
do
	character_count "$i"
done
