#!/bin/bash

DATE="${1:-2023-02-28}"
# use1
REGION="${2:-apse1}"

mkdir /home/kritarth/$DATE/

files=`find /backup-logs/$REGION/ -maxdepth 3 -type f |grep $DATE |grep plivo.log`
echo $files |xargs -d' ' -I {} /bin/bash -c "zstdgrep -a -P -o \"/Speak/ - executing with \K{.*}\" {}" > /home/kritarth/$DATE/$REGION.api.txt
