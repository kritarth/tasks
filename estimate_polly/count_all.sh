#!/bin/bash

DATE=${1:-2023-03-01}
LANG_REGEX="$2"
declare -a REGION=("use1" "usw1" "sa1" "eu1" "aps1" "apse1" "apse2")
for i in "${REGION[@]}"
do
	./count.sh $DATE $i "$LANG_REGEX"
done
